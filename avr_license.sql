-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 06, 2019 at 11:55 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `avira`
--

-- --------------------------------------------------------

--
-- Table structure for table `avr_license`
--

CREATE TABLE `avr_license` (
  `id` int(10) UNSIGNED NOT NULL,
  `license_name` varchar(255) NOT NULL,
  `customer_name` varchar(255) NOT NULL,
  `expiry_date` date NOT NULL,
  `license_details` text NOT NULL,
  `license_number` int(10) UNSIGNED NOT NULL,
  `license_key` varchar(255) NOT NULL,
  `creation_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `avr_license`
--

INSERT INTO `avr_license` (`id`, `license_name`, `customer_name`, `expiry_date`, `license_details`, `license_number`, `license_key`, `creation_date`) VALUES
(1, 'reada', 'sdasd', '2019-06-22', 'asdasd', 9966050, 'e3bf124eb7b77c4dbcdf368cb97287aad922efcc', '0000-00-00 00:00:00'),
(2, 'asdasd', 'aasda', '2019-06-30', 'asdasd', 425490, 'daab7d5cc5faaf733547ad981533b780ac9f0a4a', '2019-06-07 00:53:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `avr_license`
--
ALTER TABLE `avr_license`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `license_key` (`license_key`),
  ADD UNIQUE KEY `license_number` (`license_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `avr_license`
--
ALTER TABLE `avr_license`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
