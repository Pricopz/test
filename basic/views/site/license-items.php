<?php

/* @var $this yii\web\View */

$this->title = 'License Items';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>License Items!</h1>
    </div>

    <div class="body-content">

        <div class="row">
        <ul>
            <?php
                foreach ($licenseItems as $licenseItem) {

                    echo '<li>'  . $licenseItem->license_name . '</li>';
                }
            ?>
            </ul>
        </div>

    </div>
</div>
