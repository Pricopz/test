<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'License';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('licenseFormSubmitted')): ?>

        <div class="alert alert-success">
            License created.
        </div>

    <?php else: ?>

        <div class="row">
            <div class="col-lg-5">

                <?php $form = ActiveForm::begin(['id' => 'license-form']); ?>

                    <?= $form->field($formModel, 'license_name')->textInput(['autofocus' => true]) ?>

                    <?= $form->field($formModel, 'customer_name') ?>

                    <?= $form->field($formModel, 'expiry_date')->widget(
                                                                        \yii\jui\DatePicker::classname(), 
                                                                        [
                                                                            'dateFormat'    => 'yyyy-MM-dd',
                                                                            'options'       => [ 'class' => 'form-control', 'readonly' => 'readonly']
                                                                        ]
                                                                    ); ?>

                    <?= $form->field($formModel, 'license_details')->textarea(['rows' => 6]) ?>

                    <?= $form->field($formModel, 'verifyCode')->widget(Captcha::className(), [
                        'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                    ]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                    </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>

    <?php endif; ?>
</div>
