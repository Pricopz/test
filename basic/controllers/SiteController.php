<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\form\LicenseForm;
use app\models\db\License;
use app\services\Curl;
use yii\helpers\Json;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function beforeAction($action)
    {      
        if ($action->id == 'api-create-key-license') {
            $this->enableCsrfValidation = false;
        }
        return true;
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // $form   = new ContactForm();
        // $model  = new ContactMessage();
        // $model->email   ="test@email.com";
        // $model->message = 'Message';
        // $model->date    = new \yii\db\Expression('now()');
        // $model->save();
        return $this->render('index');
    }

    public function actionCreateLicense()
    {
        $formModel = new LicenseForm();

        if ($formModel->load(Yii::$app->request->post()) && $formModel->validate()) {
           
            $licenseName        = $formModel->license_name;  
            $customerName       = $formModel->customer_name;     
            $expiryDate         = $formModel->expiry_date;  
            $licenseDetails     = $formModel->license_details; 

            $requestURL  = 'http://localhost/' . Yii::$app->homeUrl . '/api/create-license';
            $requestData = ['name' => $customerName, 'date' => $expiryDate];
            $response    = Curl::callAPI(Curl::POST, $requestURL, $requestData);

            if ($response->code===201) {

                $data           = Json::decode($response->body);     
                $licenseNumber  = $data['message']['number'];
                $licenseKey     = $data['message']['key'];

                $license                    = new License();
                $license->license_name      = $licenseName;
                $license->customer_name     = $customerName;
                $license->expiry_date       = $expiryDate;
                $license->license_details   = $licenseDetails;
                $license->license_number    = $licenseNumber;
                $license->license_key       = $licenseKey;
                $license->creation_date     = new \yii\db\Expression('now()');
                $license->save();
    
               Yii::$app->session->setFlash('licenseFormSubmitted');
               return $this->refresh();
            }
        }

        return $this->render(
            'create-license',
            [
                'formModel' => $formModel,
            ]
        );
    }

    public function actionLicenseItems()
    {
        return $this->render(
            'license-items',
            [
                'licenseItems' => License::find()->all(),
            ]
        );
    }

    public function actionApiCreateKeyLicense()
    {
        $this->layout = '';
        if (Yii::$app->request->isPost) {
            $name 	= Yii::$app->request->post("name", null);
            $date 	= Yii::$app->request->post("date", null);

            // echo Json::encode(['number' => rand(100, 10000000), 'key' => sha1(mt_rand())]);

            return \Yii::createObject([
                'class'         => 'yii\web\Response',
                'format'        => \yii\web\Response::FORMAT_JSON,
                'statusCode'    => 201,
                'data' => [
                    'message'   => ['number' => rand(100, 10000000), 'key' => sha1(mt_rand())],
                    'code'      => 201,
                ],
            ]);
        } else {
            return \Yii::createObject([
                'class'         => 'yii\web\Response',
                'format'        => \yii\web\Response::FORMAT_JSON,
                'statusCode'    => 405,
                'data' => [
                    'message'   => 'Method not allowed',
                    'code'      => 405,
                ],
            ]);
        }
    }
}
