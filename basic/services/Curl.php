<?php
namespace app\services;

class Curl {

    const GET   = 'GET';
    const POST = 'POST';

    public static function callAPI($method, $url, $data = null)
    {
        $curl = curl_init();

        // switch $method
        switch ($method) {
            case self::POST:
                curl_setopt($curl, CURLOPT_POST, 1);

                if($data !== null) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
                }
                break;
            default:
                if ($data !== null){
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result     = curl_exec($curl);
        $errors     = curl_error($curl);
        $code   = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        
        return self::buildResponse($code, $result, $errors);
    }

    private static function buildResponse($code, $result, $errors)
    {
        $response           = new \StdClass();
        $response->code     = $code;
        $response->body     = $result;
        $response->errors    = $errors;
        return $response;
    }
}