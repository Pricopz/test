<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class LicenseForm extends Model
{
    public $license_name;
    public $customer_name;
    public $expiry_date;
    public $license_details;
    public $creation_date;
    public $verifyCode;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['license_name', 'customer_name', 'expiry_date', 'license_details'], 'required'],
            [['expiry_date'], 'date', 'format' => 'yyyy-M-d'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Verification Code',
        ];
    }
 
}
