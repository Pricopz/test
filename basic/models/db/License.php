<?php
namespace app\models\db;

use Yii;
use yii\db\ActiveRecord;

class License extends ActiveRecord
{
    /**
    $id
    $license_name
    $customer_name
    $expiry_date
    $license_details
    $license_number
    $license_key
    $creation_date 
    */

    public static function tableName()
    {
        return "avr_license";
    }

    public function rules()
    {
        return [
            [['license_name', 'customer_name', 'expiry_date', 'license_details', 'license_number', 'license_key' ], 'safe'],
        ];
    }
}